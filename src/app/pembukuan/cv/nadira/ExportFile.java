package app.pembukuan.cv.nadira;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
//import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
//import com.itextpdf.text.Image;
//import com.itextpdf.text.PageSize;
//import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
//import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class ExportFile {

    public static String PrintGenerateDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-YYYY;HH;mm;ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    public static String PrintGenerateText(int lengthText) {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = lengthText;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

//        System.out.println(generatedString);
        return generatedString;
    }

    public static PdfPCell createHeaderCell(String content, float borderWidth, int colspan, int alignment) {

        PdfPCell cell = new PdfPCell(new Phrase(content, FontFactory.getFont(FontFactory.HELVETICA_BOLD, 9)));
        cell.setBorderWidth(borderWidth);
        cell.setColspan(colspan);
//        cell.setHorizontalAlignment(alignment);
//        cell.setPaddingLeft(2);
//        cell.setPaddingRight(2);
//        cell.setPaddingTop(3);
        cell.setPaddingBottom(5);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);

        return cell;
    }

    public static PdfPCell createCell(String content, float borderWidth, int colspan, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(content, FontFactory.getFont(FontFactory.HELVETICA, 9)));
        cell.setBorderWidth(borderWidth);
        cell.setColspan(colspan);
//        cell.setHorizontalAlignment(alignment);
//        cell.setPaddingLeft(2);
//        cell.setPaddingRight(2);
//        cell.setPaddingTop(3);
        cell.setPaddingBottom(5);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//        cell.setBackgroundColor(BaseColor.DARK_GRAY);

        return cell;
    }
//
//    public static void main(String[] args) throws IOException {
//
//        Document doc = new Document(PageSize.LEGAL.rotate());
//        try {
//
////            generate a PDF at the specified location  
////          PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream("C:\Users\HP\Documents\NetBeansProjects"));
//            PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream("C:\\Users\\HP\\Documents\\NetBeansProjects\\codekan\\laporan_" + PrintGenerateText(20) + "_1.pdf"));
//            System.out.println("PDF created.");
//
//            PdfHeader event = new PdfHeader();
//            writer.setPageEvent(event);
////        opens the PDF  
//            doc.open();
////            Image img = Image.getInstance("C:\\Users\\LENOVO\\Documents\\skripsicvnadira\\logone.png");
//            Image img = Image.getInstance("C:\\Users\\HP\\Documents\\NetBeansProjects\\codekan\\skripsicvnadira\\logone.png");
//            Paragraph teksTabel1 = new Paragraph("Tabel Transaksi Masuk");
//            teksTabel1.setAlignment(Element.ALIGN_CENTER);
//            Paragraph teksTabel2 = new Paragraph("Tabel Transaksi Keluar");
//            teksTabel2.setAlignment(Element.ALIGN_CENTER);
//            Paragraph teksTabel3 = new Paragraph("Tabel Transaksi Reject");
//            teksTabel3.setAlignment(Element.ALIGN_CENTER);
//            img.setAbsolutePosition(-30f, 445f);
//            doc.add(img);
//            doc.add(new Paragraph("           Laporan Transaksi Telur CV. Nadira", FontFactory.getFont(FontFactory.HELVETICA_BOLD, 24)));
//            doc.add(new Paragraph(" "));
//            doc.add(new Paragraph(" "));
//            doc.add(new Paragraph(" "));
//            doc.add(new Paragraph("Tanggal Mulai: 13-06-2023"));
//            doc.add(new Paragraph("Tanggal Akhir: 13-07-2023"));
//            doc.add(teksTabel1);
//            doc.add(new Paragraph(" "));
////        adds paragraph to the PDF file
//            PdfPTable table = new PdfPTable(9);
//            table.setWidthPercentage(100);
////            table.setHeaderRows(2);
////            table.setWidths(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1});
//
////            String[] columns = {"Kode", "Nama", "Jenis Telur", "Status Telur", "Jenis Pembayaran", "Tanggal Transaksi", "Harga Telur", "Jumlah Telur", "Total Harga"};
//            table.addCell(createHeaderCell("Kode", 1, 1, Element.ALIGN_CENTER));
//            table.addCell(createHeaderCell("Nama", 1, 1, Element.ALIGN_CENTER));
//            table.addCell(createHeaderCell("Jenis Telur", 1, 1, Element.ALIGN_CENTER));
//            table.addCell(createHeaderCell("Status Telur", 1, 1, Element.ALIGN_CENTER));
//            table.addCell(createHeaderCell("Jenis Pembayaran", 1, 1, Element.ALIGN_CENTER));
//            table.addCell(createHeaderCell("Tanggal Transaksi", 1, 1, Element.ALIGN_CENTER));
//            table.addCell(createHeaderCell("Harga Telur", 1, 1, Element.ALIGN_CENTER));
//            table.addCell(createHeaderCell("Jumlah Telur", 1, 1, Element.ALIGN_CENTER));
//            table.addCell(createHeaderCell("Total Harga", 1, 1, Element.ALIGN_CENTER));
//
//            String[][] data = {
//                {"001", "Grego", "coklat", "masuk", "cash", "20-08-2023", "20000", "100", "2000000"},
//                {"002", "Bara", "coklat", "masuk", "cash", "20-08-2023", "20000", "100", "2000000"},
//                {"001", "Grego", "coklat", "masuk", "cash", "20-08-2023", "20000", "100", "2000000"},
//                {"002", "Bara", "coklat", "masuk", "cash", "20-08-2023", "20000", "100", "2000000"},
//                {"001", "Grego", "coklat", "masuk", "cash", "20-08-2023", "20000", "100", "2000000"},
//                {"002", "Bara", "coklat", "masuk", "cash", "20-08-2023", "20000", "100", "2000000"},
//                {"001", "Grego", "coklat", "masuk", "cash", "20-08-2023", "20000", "100", "2000000"},
//                {"002", "Bara", "coklat", "masuk", "cash", "20-08-2023", "20000", "100", "2000000"},};
//
//            for (String[] row : data) {
//                table.addCell(createCell(row[0], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[1], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[2], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[3], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[4], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[5], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[6], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[7], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[8], 1, 1, Element.ALIGN_LEFT));
//            }
//
//            doc.add(table);
//            doc.add(new Paragraph("Total Telur Masuk: 100"));
//            doc.add(new Paragraph("Total Harga Pembelian: 2.500.000"));
//            doc.add(new Paragraph(" "));
//            doc.add(teksTabel2);
//            doc.add(new Paragraph(" "));
//            doc.add(table);
//            doc.add(new Paragraph("Total Telur Keluar: 100"));
//            doc.add(new Paragraph("Total Harga Penjualan: 2.600.000"));
//            doc.add(new Paragraph(" "));
//            doc.add(teksTabel3);
//            doc.add(new Paragraph(" "));
//            doc.add(table);
//            doc.add(new Paragraph("Total Telur Reject: 0"));
//            doc.add(new Paragraph("Total Harga Reject: 0"));
//            doc.add(new Paragraph(" "));
//            doc.add(new Paragraph("Total Laba (Keluar - Masuk - Reject): 2.600.000 - 2.500.000 - 0 = 100.000"));
//            doc.add(new Paragraph(" "));
//            doc.add(new Paragraph(" "));
//            doc.add(new Paragraph("Klaten, 23 Juni 2023", FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
//            doc.add(new Paragraph("Direktur CV. Nadira", FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
//            doc.add(new Paragraph(" "));
//            doc.add(new Paragraph(" "));
//            doc.add(new Paragraph(" "));
//            doc.add(new Paragraph("Vicky Azizah Novitasari", FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
////        close the PDF file  
//            doc.close();
////        closes the writer  
//            writer.close();
//        } catch (DocumentException | FileNotFoundException e) {
//            System.out.println("Gagal Ekspor");
//        }
//
//    }

    public static class PdfHeader extends PdfPageEventHelper {

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            try {
                Rectangle pageSize = document.getPageSize();
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase(String.format("Lembar %s", String.valueOf(writer.getCurrentPageNumber()))),
                        pageSize.getRight(30), pageSize.getTop(30), 0);

            } catch (Exception e) {
            }
        }

    }
}

package app.pembukuan.cv.nadira;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbTelur {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private String buildFilterQuery(String filter1, String filter2, String filter3, String filter4) {
        StringBuilder queryBuilder = new StringBuilder("SELECT * FROM transaksi_telur WHERE ");
        // Membangun query filter berdasarkan filter yang diberikan
        if (filter1 != null) {
            queryBuilder.append("tanggal_transaksi = '").append(filter1).append("' AND ");
        }
        if (filter2 != null) {
            queryBuilder.append("jenis_telur = '").append(filter2).append("' AND ");
        }
        if (filter3 != null) {
            queryBuilder.append("status_telur = '").append(filter3).append("' AND ");
        }
        if (filter4 != null) {
            queryBuilder.append("jenis_pembayaran = '").append(filter4).append("' AND ");
        }

        // cek apakah tidak memakai filter
        if (filter1 == null && filter2 == null && filter3 == null && filter4 == null) {
            // Menghapus "WHERE" yang terakhir pada query
            queryBuilder.setLength(queryBuilder.length() - 7);
        } else {
            // Menghapus "AND" yang terakhir pada query
            queryBuilder.setLength(queryBuilder.length() - 5);
        }
        return queryBuilder.toString();
    }

    public boolean checkKode(String kode) throws ClassNotFoundException, SQLException {
        DbConnection dbConn = new DbConnection();
        java.sql.Connection conn = dbConn.getConnection();
        ResultSet rs = null;
        PreparedStatement pst = null;

        Telur telor = new Telur();
        telor.setKode(kode);
        boolean status = false;
        try {
            String sqlCheck = "select * from transaksi_telur where kode=? LIMIT 1";
            pst = conn.prepareStatement(sqlCheck);
            pst.setString(1, telor.getKode());
            rs = pst.executeQuery();
            status = rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
//                pst.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return status;
    }

    public int InsertTelur(String namaCustomer, String kode, String jenisTelur, String statusTelur, String jenisPembayaran, String tanggalTransaksi, int hargaTelur, int jumlahTelur) throws ClassNotFoundException, SQLException {
        DbConnection dbConn = new DbConnection();
        java.sql.Connection conn = dbConn.getConnection();

        Telur telor = new Telur(namaCustomer, kode, jenisTelur, statusTelur, jenisPembayaran, tanggalTransaksi, hargaTelur, jumlahTelur);

        Statement st = null;
        int result = 0;
        try {
            String SQL = "insert into transaksi_telur (`kode`, `nama_customer`, `jenis_telur`, `status_telur`, `jenis_pembayaran`, `tanggal_transaksi`, `harga_telur`, `jumlah_telur`, `total_harga`) "
                    + "values('" + telor.getKode()
                    + "','" + telor.getNamaCustomer()
                    + "','" + telor.getJenisTelur()
                    + "','" + telor.getStatusTelur()
                    + "','" + telor.getJenisPembayaran()
                    + "','" + telor.getTanggalTransaksi()
                    + "','" + telor.getHargaTelur()
                    + "','" + telor.getJumlahTelur()
                    + "','" + telor.getTotalHarga()
                    + "')";

            st = conn.createStatement();
            result = st.executeUpdate(SQL);

        } catch (SQLException ex) {
            Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
//                st.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return result;
    }

    public ArrayList<Telur> getAllData() throws SQLException, ClassNotFoundException {
        ArrayList<Telur> listTelur = new ArrayList<>();

        DbConnection dbConn = new DbConnection();
        Connection conn = dbConn.getConnection();
        Statement st = null;
        ResultSet rs = null;

        String sql = "select * from transaksi_telur";

        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
                String dateStr = rs.getString(7);
                SimpleDateFormat sdfFrom = new SimpleDateFormat("yyyy-MM-dd");
                Date date = sdfFrom.parse(dateStr);
                SimpleDateFormat sdfTo = new SimpleDateFormat("dd-MMMM-yyyy");
                String formattedDate = sdfTo.format(date);

                Telur telor = new Telur();
                telor.setId(rs.getInt(1));
                telor.setKode(rs.getString(2));
                telor.setNamaCustomer(rs.getString(3));
                telor.setJenisTelur(rs.getString(4));
                telor.setStatusTelur(rs.getString(5));
                telor.setJenisPembayaran(rs.getString(6));
                telor.setTanggalTransaksi(formattedDate);
                telor.setHargaTelur(rs.getInt(8));
                telor.setJumlahTelur(rs.getInt(9));
                telor.setTotalHarga(rs.getInt(9), rs.getInt(8));

                listTelur.add(telor);
            }
        } catch (SQLException | ParseException e) {
            Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
//                st.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listTelur;
    }

    public ArrayList<Telur> getDataByKode(String kode) throws SQLException, ClassNotFoundException {
        ArrayList<Telur> listTelur = new ArrayList<>();
        DbConnection dbConn = new DbConnection();
        Connection conn = dbConn.getConnection();
        Statement st = null;
        ResultSet rs = null;

        String sql = "select * from transaksi_telur where kode = '" + kode + "'";

        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
                String dateStr = rs.getString(7);
                SimpleDateFormat sdfFrom = new SimpleDateFormat("yyyy-MM-dd");
                Date date = sdfFrom.parse(dateStr);
                SimpleDateFormat sdfTo = new SimpleDateFormat("dd-MMMM-yyyy");
                String formattedDate = sdfTo.format(date);

                Telur telor = new Telur();
                telor.setId(rs.getInt(1));
                telor.setKode(rs.getString(2));
                telor.setNamaCustomer(rs.getString(3));
                telor.setJenisTelur(rs.getString(4));
                telor.setStatusTelur(rs.getString(5));
                telor.setJenisPembayaran(rs.getString(6));
                telor.setTanggalTransaksi(formattedDate);
                telor.setHargaTelur(rs.getInt(8));
                telor.setJumlahTelur(rs.getInt(9));
                telor.setTotalHarga(rs.getInt(9), rs.getInt(8));

                listTelur.add(telor);
            }
        } catch (SQLException | ParseException e) {
            Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
//                st.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listTelur;
    }

    public int UpdateTelur(int id, String kode, String namaCustomer, String jenisTelur, String statusTelur, String jenisPembayaran, String tanggalTransaksi, int hargaTelur, int jumlahTelur) throws SQLException, ClassNotFoundException {
        DbConnection dbConn = new DbConnection();
        java.sql.Connection conn = dbConn.getConnection();
        PreparedStatement pst = null;

        int result = 0;
        Telur telor = new Telur(namaCustomer, kode, jenisTelur, statusTelur, jenisPembayaran, tanggalTransaksi, hargaTelur, jumlahTelur);
        telor.setId(id);

        try {
            String sql = "UPDATE transaksi_telur SET "
                    + "kode = ?, "
                    + "nama_customer = ?, "
                    + "jenis_telur = ?, "
                    + "status_telur = ?, "
                    + "jenis_pembayaran = ?, "
                    + "tanggal_transaksi = ?, "
                    + "harga_telur = ?, "
                    + "jumlah_telur = ?, "
                    + "total_harga = ? "
                    + "WHERE id = ?";
            pst = conn.prepareStatement(sql);
            pst.setString(1, telor.getKode());
            pst.setString(2, telor.getNamaCustomer());
            pst.setString(3, telor.getJenisTelur());
            pst.setString(4, telor.getStatusTelur());
            pst.setString(5, telor.getJenisPembayaran());
            pst.setString(6, telor.getTanggalTransaksi());
            pst.setInt(7, telor.getHargaTelur());
            pst.setInt(8, telor.getJumlahTelur());
            pst.setInt(9, telor.getTotalHarga());
            pst.setInt(10, telor.getId());
            result = pst.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
//                pst.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }

    public int DeleteTelur(String kode) throws SQLException, ClassNotFoundException {
        DbConnection dbConn = new DbConnection();
        java.sql.Connection conn = dbConn.getConnection();

        Statement st = null;
        int result = 0;
        Telur telor = new Telur();
        telor.setKode(kode);
        try {
            st = conn.createStatement();
            String sql = "DELETE FROM `transaksi_telur` WHERE kode = '" + telor.getKode() + "'";
            result = st.executeUpdate(sql);

        } catch (SQLException ex) {
            Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
//                st.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return result;
    }

    public ArrayList<Telur> getDataByFilter(String filterTanggalTransaksi, String filterJenisTelur, String filterStatusTelur, String filterJenisPembayaran) throws SQLException, ClassNotFoundException {
        ArrayList<Telur> listTelur = new ArrayList<>();
        DbConnection dbConn = new DbConnection();
        java.sql.Connection conn = dbConn.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        try {
            String sql = buildFilterQuery(filterTanggalTransaksi, filterJenisTelur, filterStatusTelur, filterJenisPembayaran);
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();

//            System.out.println("pst = " + pst);

            if (rs != null) {
                while (rs.next()) {
                    String dateStr = rs.getString(7);
                    SimpleDateFormat sdfFrom = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = sdfFrom.parse(dateStr);
                    SimpleDateFormat sdfTo = new SimpleDateFormat("dd-MMMM-yyyy");
                    String formattedDate = sdfTo.format(date);

                    Telur telor = new Telur();
                    telor.setId(rs.getInt(1));
                    telor.setKode(rs.getString(2));
                    telor.setNamaCustomer(rs.getString(3));
                    telor.setJenisTelur(rs.getString(4));
                    telor.setStatusTelur(rs.getString(5));
                    telor.setJenisPembayaran(rs.getString(6));
                    telor.setTanggalTransaksi(formattedDate);
                    telor.setHargaTelur(rs.getInt(8));
                    telor.setJumlahTelur(rs.getInt(9));
                    telor.setTotalHarga(rs.getInt(9), rs.getInt(8));

                    listTelur.add(telor);
                }
            } else {
                listTelur = null;
            }
        } catch (SQLException | ParseException e) {
            Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
//                pst.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return listTelur;
    }

    public ArrayList<Telur> getDataLaporan(String statusTelur, String tglMulai, String tglSelesai, String jenisTelur) throws SQLException, ClassNotFoundException {
        ArrayList<Telur> listTelur = new ArrayList<>();
        DbConnection dbConn = new DbConnection();
        java.sql.Connection conn = dbConn.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        try {
            String sql = "SELECT * FROM `transaksi_telur` WHERE status_telur = ? AND "
                    + "tanggal_transaksi BETWEEN ? AND ? "
                    + "AND jenis_telur = ?";
            pst = conn.prepareStatement(sql);
            pst.setString(1, statusTelur);
            pst.setString(2, tglMulai);
            pst.setString(3, tglSelesai);
            pst.setString(4, jenisTelur);
            rs = pst.executeQuery();
//            System.out.println("query = " + pst);

            if (!rs.isBeforeFirst()) {
                listTelur = null;
            } else {
                while (rs.next()) {
                    String dateStr = rs.getString(7);
                    SimpleDateFormat sdfFrom = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = sdfFrom.parse(dateStr);
                    SimpleDateFormat sdfTo = new SimpleDateFormat("dd-MMMM-yyyy");
                    String formattedDate = sdfTo.format(date);

                    Telur telor = new Telur();
                    telor.setId(rs.getInt(1));
                    telor.setKode(rs.getString(2));
                    telor.setNamaCustomer(rs.getString(3));
                    telor.setJenisTelur(rs.getString(4));
                    telor.setStatusTelur(rs.getString(5));
                    telor.setJenisPembayaran(rs.getString(6));
                    telor.setTanggalTransaksi(formattedDate);
                    telor.setHargaTelur(rs.getInt(8));
                    telor.setJumlahTelur(rs.getInt(9));
                    telor.setTotalHarga(rs.getInt(9), rs.getInt(8));

                    listTelur.add(telor);
                }
            }
        } catch (SQLException | ParseException e) {
            Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
//                pst.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return listTelur;
    }

    public Map<String, Integer> getDataLaporanTotal(String statusTelur, String tglMulai, String tglSelesai, String jenisTelur) throws SQLException, ClassNotFoundException {
        DbConnection dbConn = new DbConnection();
        java.sql.Connection conn = dbConn.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        int sumJumlahTelur = 0;
        int sumTotalHarga = 0;
        Map<String, Integer> result = new HashMap<>();

        try {
            String sql = "SELECT sum(jumlah_telur), sum(total_harga) FROM `transaksi_telur` WHERE status_telur = ? AND "
                    + "tanggal_transaksi BETWEEN ? AND ? "
                    + "AND jenis_telur = ?";
            pst = conn.prepareStatement(sql);
            pst.setString(1, statusTelur);
            pst.setString(2, tglMulai);
            pst.setString(3, tglSelesai);
            pst.setString(4, jenisTelur);
            rs = pst.executeQuery();
//            System.out.println("query = " + pst);

            while (rs.next()){
                sumJumlahTelur = (rs.getString(1) != null) ? rs.getInt(1) : 0;
                sumTotalHarga = (rs.getString(2) != null) ? rs.getInt(2) : 0;
            }
            
        } catch (SQLException e) {
            Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
//                pst.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        result.put("sumJumlahTelur", sumJumlahTelur);
        result.put("sumTotalHarga", sumTotalHarga);
        return result;
    }

    public Integer getDataLaporanTotalJumlahTelur(String statusTelur, String tglMulai, String tglSelesai, String jenisTelur){
        DbConnection dbConn = new DbConnection();
        java.sql.Connection conn = dbConn.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        int sumJumlahTelur = 0;
        
        try {
            String sql = "SELECT sum(jumlah_telur) FROM `transaksi_telur` WHERE status_telur = ? AND "
                    + "tanggal_transaksi BETWEEN ? AND ? "
                    + "AND jenis_telur = ?";
            pst = conn.prepareStatement(sql);
            pst.setString(1, statusTelur);
            pst.setString(2, tglMulai);
            pst.setString(3, tglSelesai);
            pst.setString(4, jenisTelur);
            rs = pst.executeQuery();
//            System.out.println("query = " + pst);

            while (rs.next()){
                sumJumlahTelur = rs.getInt(1);
            }
            
        } catch (SQLException e) {
            Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
//                pst.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return sumJumlahTelur;
    }
    
    public Integer getDataLaporanTotalTotalHarga(String statusTelur, String tglMulai, String tglSelesai, String jenisTelur){
        DbConnection dbConn = new DbConnection();
        java.sql.Connection conn = dbConn.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        int sumTotalHarga = 0;
        
        try {
            String sql = "SELECT sum(total_harga) FROM `transaksi_telur` WHERE status_telur = ? AND "
                    + "tanggal_transaksi BETWEEN ? AND ? "
                    + "AND jenis_telur = ?";
            pst = conn.prepareStatement(sql);
            pst.setString(1, statusTelur);
            pst.setString(2, tglMulai);
            pst.setString(3, tglSelesai);
            pst.setString(4, jenisTelur);
            rs = pst.executeQuery();
//            System.out.println("query = " + pst);

            while (rs.next()){
                sumTotalHarga = rs.getInt(1);
            }
            
        } catch (SQLException e) {
            Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
//                pst.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return sumTotalHarga;
    }
    
}

package app.pembukuan.cv.nadira;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Connection {
    java.sql.Connection con = null;
    ResultSet rs = null;
    PreparedStatement pst = null;
    Statement statBrg;
    Boolean ada = false;
    
    public void koneksi() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost/java_transaksi"; //url database
            String user = "root"; //user database
            String pass = ""; //password database
            con = DriverManager.getConnection(url, user, pass);
//            statBrg = con.createStatement(rs.TYPE_SCROLL_SENSITIVE, rs.CONCUR_UPDATABLE);
//            rs = statBrg.executeQuery("select * from transaksi_telur");
        }catch(ClassNotFoundException | SQLException e){
            System.out.println(e);
        }
    }
}

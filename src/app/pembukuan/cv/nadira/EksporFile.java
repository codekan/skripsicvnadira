package app.pembukuan.cv.nadira;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Random;

public class EksporFile {

    public static String PrintGenerateText(int lengthText) {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = lengthText;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

//        System.out.println(generatedString);
        return generatedString;
    }

    public static PdfPCell createCell(String content, float borderWidth, int colspan, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(content));
        cell.setBorderWidth(borderWidth);
        cell.setColspan(colspan);
//        cell.setHorizontalAlignment(alignment);
//        cell.setPaddingLeft(2);
//        cell.setPaddingRight(2);
//        cell.setPaddingTop(3);
        cell.setPaddingBottom(5);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBackgroundColor(BaseColor.DARK_GRAY);

        return cell;
    }

    public static void main(String[] args) {
        PreparedStatement pst = null;
        ResultSet rs = null;
        DbConnection dbConn = new DbConnection();
        java.sql.Connection conn = dbConn.getConnection();
        Document doc = new Document(PageSize.LEGAL.rotate());
        try {
            // query get data
            String sql = "select * from transaksi_telur";
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            //generate a PDF at the specified location  
//            PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream("C:\Users\HP\Documents\NetBeansProjects"));
            PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream("C:\\Users\\HP\\Documents\\NetBeansProjects\\generate_" + PrintGenerateText(20) + "_1.pdf"));
            System.out.println("PDF created.");
//        opens the PDF  
            doc.open();
//        adds paragraph to the PDF file
            PdfPTable table = new PdfPTable(9);
            table.setWidthPercentage(100);
//            table.setHeaderRows(2);
//            table.setWidths(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1});

//            String[] columns = {"Kode", "Nama", "Jenis Telur", "Status Telur", "Jenis Pembayaran", "Tanggal Transaksi", "Harga Telur", "Jumlah Telur", "Total Harga"};
            table.addCell(createCell("Kode", 1, 1, Element.ALIGN_CENTER));
            table.addCell(createCell("Nama", 1, 1, Element.ALIGN_CENTER));
            table.addCell(createCell("Jenis Telur", 1, 1, Element.ALIGN_CENTER));
            table.addCell(createCell("Status Telur", 1, 1, Element.ALIGN_CENTER));
            table.addCell(createCell("Jenis Pembayaran", 1, 1, Element.ALIGN_CENTER));
            table.addCell(createCell("Tanggal Transaksi", 1, 1, Element.ALIGN_CENTER));
            table.addCell(createCell("Harga Telur", 1, 1, Element.ALIGN_CENTER));
            table.addCell(createCell("Jumlah Telur", 1, 1, Element.ALIGN_CENTER));
            table.addCell(createCell("Total Harga", 1, 1, Element.ALIGN_CENTER));

//            String[][] data = {
//                {"001", "Grego", "coklat", "masuk", "cash", "20-08-2023", "20000", "100", "2000000"},
//                {"002", "Bara", "coklat", "masuk", "cash", "20-08-2023", "20000", "100", "2000000"}
//            };
//            for (String[] row : data) {
//                table.addCell(createCell(row[0], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[1], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[2], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[3], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[4], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[5], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[6], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[7], 1, 1, Element.ALIGN_LEFT));
//                table.addCell(createCell(row[8], 1, 1, Element.ALIGN_LEFT));
//            }
            while(rs.next()){
                table.addCell(createCell(rs.getString(2), 1, 1, Element.ALIGN_LEFT));
                table.addCell(createCell(rs.getString(3), 1, 1, Element.ALIGN_LEFT));
                table.addCell(createCell(rs.getString(4), 1, 1, Element.ALIGN_LEFT));
                table.addCell(createCell(rs.getString(5), 1, 1, Element.ALIGN_LEFT));
                table.addCell(createCell(rs.getString(6), 1, 1, Element.ALIGN_LEFT));
                table.addCell(createCell(rs.getString(7), 1, 1, Element.ALIGN_LEFT));
                table.addCell(createCell(rs.getString(8), 1, 1, Element.ALIGN_LEFT));
                table.addCell(createCell(rs.getString(9), 1, 1, Element.ALIGN_LEFT));
                table.addCell(createCell(rs.getString(10), 1, 1, Element.ALIGN_LEFT));
            }
//            table.addCell(createCell("Totals", 2, 4, Element.ALIGN_LEFT));
//            table.addCell(createCell("$1,552.00", 2, 1, Element.ALIGN_RIGHT));
            doc.add(table);

//        close the PDF file  
            doc.close();
//        closes the writer  
            writer.close();
            // close connection
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("Gagal Ekspor");
        }

    }

}

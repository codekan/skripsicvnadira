package app.pembukuan.cv.nadira;

public class Telur {
    
    private String namaCustomer, kode, jenisTelur, statusTelur, jenisPembayaran, tanggalTransaksi;
    private int id, hargaTelur, jumlahTelur, totalHarga;

    public Telur(){
    
    }
    
    public Telur(String namaCustomer, String kode, String jenisTelur, String statusTelur, String jenisPembayaran, String tanggalTransaksi, int hargaTelur, int jumlahTelur){
        this.tanggalTransaksi = tanggalTransaksi;
        this.namaCustomer = namaCustomer;
        this.kode = kode;
        this.jenisTelur = jenisTelur;
        this.hargaTelur = hargaTelur;
        this.jumlahTelur = jumlahTelur;
        this.totalHarga = jumlahTelur*hargaTelur;
        this.statusTelur = statusTelur;
        this.jenisPembayaran = jenisPembayaran;
    }
    
    public String getTanggalTransaksi() {
        return tanggalTransaksi;
    }

    public void setTanggalTransaksi(String tanggalTransaksi) {
        this.tanggalTransaksi = tanggalTransaksi;
    }

    public String getNamaCustomer() {
        return namaCustomer;
    }

    public void setNamaCustomer(String namaCustomer) {
        this.namaCustomer = namaCustomer;
    }
     
    public String getKode() {
        return kode;
    }

    public void setKode(String kodeTelur) {
        this.kode = kodeTelur;
    }
    
     public String getJenisTelur() {
        return jenisTelur;
    }

    public void setJenisTelur(String jenisTelur) {
        this.jenisTelur = jenisTelur;
    }

    public int getHargaTelur() {
        return hargaTelur;
    }

    public void setHargaTelur(int hargaTelur) {
        this.hargaTelur = hargaTelur;
    }
    
    public int getJumlahTelur() {
        return jumlahTelur;
    }

    public void setJumlahTelur(int jumlahTelur) {
        this.jumlahTelur = jumlahTelur;
    }

    public int getTotalHarga() {
        return totalHarga;
    }

    public void setTotalHarga(int jmlTelur, int hrgTelur) {
        int ttlTelur;
        ttlTelur = jmlTelur*hrgTelur;
        this.totalHarga = ttlTelur;
    }

    public String getStatusTelur() {
        return statusTelur;
    }

    public void setStatusTelur(String statusTelur) {
        this.statusTelur = statusTelur;
    }

    public String getJenisPembayaran() {
        return jenisPembayaran;
    }

    public void setJenisPembayaran(String jenisPembayaran) {
        this.jenisPembayaran = jenisPembayaran;
    }
    
    public int getId(){
        return id;
    }
    
    public void setId(int id){
        this.id = id;
    }
}

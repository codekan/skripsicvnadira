package app.pembukuan.cv.nadira;

import javax.swing.JOptionPane;
import javax.swing.UnsupportedLookAndFeelException;

public class AppPembukuanCVNadira {

    /**
     * @param args
     */
    public static void main(String[] args) {
        DbConnection dbConn = new DbConnection();
        java.sql.Connection conn = dbConn.getConnection();
//        System.out.println(conn);
        if (conn != null) {
            // untuk menyamakan tema Nimbus di JFrame
            try {
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Nimbus".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException ex) {
            }
            // panggil JFrame
            Login login = new Login();
            login.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Database belum tersambung !", "Alert !", JOptionPane.ERROR_MESSAGE);
        }
    }

}

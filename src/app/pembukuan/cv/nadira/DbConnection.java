package app.pembukuan.cv.nadira;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbConnection {
    private final String username="root";
    private final String password="";
    private final String url="jdbc:mysql://localhost/java_transaksi";
    private final String port="3306";
    
//    private final String username="sql9640476";
//    private final String password="F6iaAQ7Jul";
//    private final String port="3306";
//    private final String url="jdbc:mysql://sql9.freesqldatabase.com:3306/sql9640476";
    
    public java.sql.Connection getConnection(){
        java.sql.Connection conn=null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url,username,password);
        }catch(ClassNotFoundException | SQLException ex){
            Logger.getLogger(DbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return conn;
    }
}

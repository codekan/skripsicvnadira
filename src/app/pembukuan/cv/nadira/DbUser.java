package app.pembukuan.cv.nadira;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbUser {

    public boolean UserLogin(String username, String password) throws ClassNotFoundException, SQLException {
        DbConnection dbConn = new DbConnection();
        java.sql.Connection conn = dbConn.getConnection();
        ResultSet rs = null;
        PreparedStatement pst = null;

        boolean status = false;
        try {
            String sql = "SELECT * FROM users WHERE username = ? AND password = md5(?) LIMIT 1";
            pst = conn.prepareStatement(sql);
            pst.setString(1, username);
            pst.setString(2, password);
            rs = pst.executeQuery();
            status = rs.next();
        } catch (SQLException e) {
            Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
//                pst.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppPembukuanCVNadira.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return status;
    }
}

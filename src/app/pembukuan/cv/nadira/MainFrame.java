package app.pembukuan.cv.nadira;

import static app.pembukuan.cv.nadira.ExportFile.PrintGenerateDate;
//import static app.pembukuan.cv.nadira.ExportFile.PrintGenerateText;
import static app.pembukuan.cv.nadira.ExportFile.createCell;
import static app.pembukuan.cv.nadira.ExportFile.createHeaderCell;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.toedter.calendar.JDateChooser;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
//import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
//import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableModel;

public final class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainFrame
     *
     * @param originalData
     * @param numberOfRowWhereToInsertData
     * @param dataToInsert
     * @return
     */
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    // panggil object CRUD
    DbTelur sqlTelur = new DbTelur();

    public String[][] insertRow(String[][] originalData, int numberOfRowWhereToInsertData, String[] dataToInsert) {
        String[][] output = new String[originalData.length + 1][];
        if (numberOfRowWhereToInsertData >= 0) {
            System.arraycopy(originalData, 0, output, 0, numberOfRowWhereToInsertData);
        }
        output[numberOfRowWhereToInsertData] = dataToInsert;
        if (output.length - (numberOfRowWhereToInsertData + 1) >= 0) {
            System.arraycopy(originalData, numberOfRowWhereToInsertData + 1 - 1, output, numberOfRowWhereToInsertData + 1, output.length - (numberOfRowWhereToInsertData + 1));
        }
        return output;

    }

    public static class PdfHeader extends PdfPageEventHelper {
        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            try {
                Rectangle pageSize = document.getPageSize();
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase(String.format("Lembar %s", String.valueOf(writer.getCurrentPageNumber()))),
                        pageSize.getRight(30), pageSize.getTop(30), 0);
            } catch (Exception e) {
            }
        }
    }

    public void DisplayDataTables() {
        // untuk menampilkan isi data ke JTable
        try {
            DefaultTableModel tableModel = new DefaultTableModel();
            String[] columnNames = {"Kode", "Nama Customer", "Jenis Telur", "Status Telur", "Jenis Pembayaran", "Tanggal Transaksi", "Harga Telur", "Jumlah Telur", "Total Harga Telur"};
            for (String columnName : columnNames) {
                tableModel.addColumn(columnName);
            }
            ArrayList<Telur> lists = sqlTelur.getAllData();
            for (Telur telur : lists) {
                Object[] row = {telur.getKode(),telur.getNamaCustomer(),telur.getJenisTelur(),telur.getStatusTelur(),telur.getJenisPembayaran(),telur.getTanggalTransaksi(),telur.getHargaTelur(),telur.getJumlahTelur(),telur.getTotalHarga()};
                tableModel.addRow(row);
            }
            tabelTelur.setModel(tableModel);
        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void clearFormTambah() {
        // untuk membersihkan form tambah
        kode.setText(null);
        namaCustomer.setText(null);
        jenisTelur.setSelectedIndex(0);
        jumlahTelur.setValue(0);
        hargaTelur.setValue(0);
        jenisPembayaran.setSelectedIndex(0);
        statusTelur.setSelectedIndex(0);
        tanggalTransaksi.setDate(null);
    }

    public int findIndex(String[] array, String input) {
        // untuk mencari index
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(input)) {
                // Data ditemukan dalam array
                return i;
            }
        }
        // Data tidak ditemukan dalam array
        return -1;
    }

    public MainFrame() {
        initComponents();
        this.setTitle("Pembukuan Order CV Nadira");
        // agar otomatis tengah layar saat running
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        // tampilkan data di JTable
        DisplayDataTables();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        label1 = new java.awt.Label();
        jLabel11 = new javax.swing.JLabel();
        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        namaCustomer = new javax.swing.JTextField();
        jumlahTelur = new javax.swing.JSpinner();
        hargaTelur = new javax.swing.JSpinner();
        statusTelur = new javax.swing.JComboBox<>();
        jenisPembayaran = new javax.swing.JComboBox<>();
        tombolTambah = new javax.swing.JButton();
        jenisTelur = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        kode = new javax.swing.JTextField();
        tanggalTransaksi = new com.toedter.calendar.JDateChooser();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelTelur = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        filterJenisTelur = new javax.swing.JComboBox<>();
        tombolFilter = new javax.swing.JButton();
        filterJenisPembayaran = new javax.swing.JComboBox<>();
        filterStatusTelur = new javax.swing.JComboBox<>();
        filterTanggalTransaksi = new com.toedter.calendar.JDateChooser();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        eksporTabel = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        buatLaporan = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        PanduanPengguna = new javax.swing.JMenuItem();

        jMenuItem1.setText("jMenuItem1");

        jMenuItem2.setText("jMenuItem2");

        label1.setText("label1");

        jLabel11.setText("jLabel11");

        jMenu3.setText("File");
        jMenuBar2.add(jMenu3);

        jMenu4.setText("Edit");
        jMenuBar2.add(jMenu4);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(102, 255, 102));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Form Transaksi Telur CV. Nadira");

        jLabel2.setText("Nama");

        jLabel3.setText("Jumlah Telur");

        jLabel4.setText("Harga @kg");

        jLabel5.setText("Jenis Telur");

        jLabel6.setText("Status");

        jLabel7.setText("Metode Pembayaran");

        namaCustomer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 204, 204)));

        jumlahTelur.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 204, 204)));

        hargaTelur.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 204, 204)));

        statusTelur.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Masuk", "Keluar", "Reject" }));

        jenisPembayaran.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tunai Cash", "Transfer Bank" }));

        tombolTambah.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tombolTambah.setText("Simpan");
        tombolTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolTambahActionPerformed(evt);
            }
        });

        jenisTelur.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Coklat", "Putih" }));

        jLabel9.setText("Tanggal");

        jLabel10.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setText("Kode");

        kode.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 204, 204)));

        tanggalTransaksi.setBackground(new java.awt.Color(255, 255, 255));
        tanggalTransaksi.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 204, 204)));
        tanggalTransaksi.setForeground(new java.awt.Color(51, 153, 255));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tombolTambah, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(namaCustomer)
                                    .addComponent(hargaTelur)
                                    .addComponent(jumlahTelur)
                                    .addComponent(jenisTelur, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(statusTelur, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jenisPembayaran, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(kode)
                                    .addComponent(tanggalTransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(23, 23, 23))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1)
                .addGap(27, 27, 27)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(kode, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(namaCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jumlahTelur, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(hargaTelur, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jenisTelur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusTelur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jenisPembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tanggalTransaksi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tombolTambah, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        namaCustomer.getAccessibleContext().setAccessibleDescription("");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tabelTelur.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Tanggal", "Nama Supplier", "Jenis Telur", "Jumlah Telur", "Harga Telur", "Status Telur", "Jenis Pembayaran", "Total"
            }
        ));
        tabelTelur.setGridColor(new java.awt.Color(0, 0, 0));
        tabelTelur.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tabelTelurMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tabelTelur);

        jScrollPane2.setViewportView(jScrollPane1);

        jLabel8.setBackground(new java.awt.Color(0, 0, 0));
        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setText("Tabel Transaksi Telur CV. Nadira");

        filterJenisTelur.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "(Pilih Jenis Telur)", "Coklat", "Putih" }));

        tombolFilter.setText("Saring");
        tombolFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolFilterActionPerformed(evt);
            }
        });

        filterJenisPembayaran.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "(Pilih Jenis Pembayaran)", "Tunai Cash", "Transfer Bank" }));

        filterStatusTelur.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "(Pilih Status Telur)", "Masuk", "Keluar", "Reject" }));

        filterTanggalTransaksi.setBackground(new java.awt.Color(255, 255, 255));
        filterTanggalTransaksi.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 204, 204)));
        filterTanggalTransaksi.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(filterTanggalTransaksi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(filterJenisTelur, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(filterStatusTelur, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(filterJenisPembayaran, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(30, 30, 30)
                                .addComponent(tombolFilter, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(47, 47, 47)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(filterJenisPembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tombolFilter, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(filterStatusTelur, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(filterJenisTelur, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(filterTanggalTransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        filterJenisTelur.getAccessibleContext().setAccessibleDescription("");
        filterJenisPembayaran.getAccessibleContext().setAccessibleName("");
        filterJenisPembayaran.getAccessibleContext().setAccessibleDescription("");
        filterStatusTelur.getAccessibleContext().setAccessibleName("");
        filterStatusTelur.getAccessibleContext().setAccessibleDescription("");
        filterTanggalTransaksi.getAccessibleContext().setAccessibleName("");
        filterTanggalTransaksi.getAccessibleContext().setAccessibleDescription("");

        jPanel3.setBackground(new java.awt.Color(90, 122, 172));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/app/pembukuan/cv/nadira/logone.png"))); // NOI18N

        jLabel13.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel13.setText("CV. Nadira");

        jLabel14.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel14.setText("Kantolan RT. 003 RW. 002, Pakahan, Jogonalan, Klaten 57452 ");

        jLabel15.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel15.setText("nadirasan.telur@gmail.com");

        jLabel16.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel16.setText("+62857-2685-3935");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel14)
                        .addGap(2, 2, 2)
                        .addComponent(jLabel15)
                        .addGap(4, 4, 4)
                        .addComponent(jLabel16))
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 20, Short.MAX_VALUE))
        );

        jMenu1.setText("File");

        eksporTabel.setText("Ekspor Tabel");
        eksporTabel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eksporTabelActionPerformed(evt);
            }
        });
        jMenu1.add(eksporTabel);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Laporan");

        buatLaporan.setText("Buat Laporan");
        buatLaporan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buatLaporanActionPerformed(evt);
            }
        });
        jMenu2.add(buatLaporan);

        jMenuBar1.add(jMenu2);

        jMenu5.setText("Bantuan");

        PanduanPengguna.setText("Panduan Pengguna");
        PanduanPengguna.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PanduanPenggunaActionPerformed(evt);
            }
        });
        jMenu5.add(PanduanPengguna);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.getAccessibleContext().setAccessibleName("");
        jPanel2.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tombolTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolTambahActionPerformed
        if (kode.getText().isEmpty() || namaCustomer.getText().isEmpty() || tanggalTransaksi.getDate() == null) {
            JOptionPane.showMessageDialog(null, "mohon untuk isi semua form");
        } else {
            try {
                boolean status = sqlTelur.checkKode(kode.getText());
                if (status) {
//                    System.out.println("Status " + status);
//                    System.out.println("Ada kode yg sama");
                    JOptionPane.showMessageDialog(null, "Ada Kode Yang Sama", "Alert !", JOptionPane.ERROR_MESSAGE);
                } else {
//                    System.out.println("Status " + status);
//                    System.out.println("tidak ada kode yg sama");
                    try {
                        // proses insert data
                        int row = sqlTelur.InsertTelur(namaCustomer.getText(), kode.getText(), jenisTelur.getSelectedItem().toString(), statusTelur.getSelectedItem().toString(), jenisPembayaran.getSelectedItem().toString(), sdf.format(tanggalTransaksi.getDate()), hargaTelur.getValue().hashCode(), jumlahTelur.getValue().hashCode());
                        if (row > 0) {
                            JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
                            // tampilkan data di JTable
                            DisplayDataTables();
                            // menghapus form setelah berhasil menambah
                            clearFormTambah();
                        } else {
                            JOptionPane.showMessageDialog(null, "Something goes wrong", "Alert !", JOptionPane.ERROR_MESSAGE);
                        }
                    } catch (HeadlessException | ClassNotFoundException | SQLException exc) {
                        System.err.println(exc.getMessage());
                        JOptionPane.showMessageDialog(null, "Something goes wrong", "Alert !", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } catch (HeadlessException | ClassNotFoundException | SQLException e) {
                System.err.println(e.getMessage());
                JOptionPane.showMessageDialog(null, "Something goes wrong", "Alert !", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_tombolTambahActionPerformed

    private void tabelTelurMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelTelurMousePressed
        String[] tombolKonfirmasiEditHapus = {"Edit", "Hapus", "Cancel"};

        // menampilkan JOptionPane Dialog
        int hasilKonfirmasiEditHapus = JOptionPane.showOptionDialog(null, "Edit/Hapus data ?", "Konfirmasi",
                JOptionPane.WARNING_MESSAGE, 0, null, tombolKonfirmasiEditHapus, tombolKonfirmasiEditHapus[2]);

        if (hasilKonfirmasiEditHapus == 0) {
            // jika ingin mengubah data
            String listJenisTelur[] = {"Coklat", "Putih"};
            String listJenisPembayaran[] = {"Tunai Cash", "Transfer Bank"};
            String listStatusTelur[] = {"Masuk", "Keluar", "Reject"};

            JTextField kode1 = new JTextField();
            kode1.setColumns(50);
            JTextField namaCustomer1 = new JTextField();
            JComboBox jenisTelur1 = new JComboBox(listJenisTelur);
            JSpinner jumlahTelur1 = new JSpinner();
            JSpinner hargaTelur1 = new JSpinner();
            JComboBox jenisPembayaran1 = new JComboBox(listJenisPembayaran);
            JComboBox statusTelur1 = new JComboBox(listStatusTelur);
            JDateChooser tanggalTransaksi1 = new JDateChooser();

            int getIdTelur = 0;
            Point point = evt.getPoint();
            int row = tabelTelur.rowAtPoint(point);
            String valueInCell = (String) tabelTelur.getValueAt(row, 0);
            try {
//                System.out.println("valueInCell = " + valueInCell);
                ArrayList<Telur> list = sqlTelur.getDataByKode(valueInCell);
                for (Telur telur : list) {
                    getIdTelur = telur.getId();
                    kode1.setText(telur.getKode());
                    namaCustomer1.setText(telur.getNamaCustomer());
                    jenisTelur1.setSelectedIndex(findIndex(listJenisTelur, telur.getJenisTelur()));
                    jumlahTelur1.setValue(telur.getJumlahTelur());
                    hargaTelur1.setValue(telur.getHargaTelur());
                    jenisPembayaran1.setSelectedIndex(findIndex(listJenisPembayaran, telur.getJenisPembayaran()));
                    statusTelur1.setSelectedIndex(findIndex(listStatusTelur, telur.getStatusTelur()));
                    SimpleDateFormat sdfFrom = new SimpleDateFormat("dd-MMMM-yyyy");
                    Date date = sdfFrom.parse(telur.getTanggalTransaksi());
                    tanggalTransaksi1.setDate(date);
                }
            } catch (ClassNotFoundException | SQLException | ParseException e) {
                System.out.println(e.getMessage());
            }
            Object[] komponenFormEdit = {
                "Kode", kode1,
                "Nama Supplier:", namaCustomer1, //            
                "Jenis Telur:", jenisTelur1,
                "Jumlah Telur:", jumlahTelur1,
                "Harga Telur:", hargaTelur1,
                "Jenis Pembayaran", jenisPembayaran1,
                "Status Telur", statusTelur1,
                "Tanggal", tanggalTransaksi1
            };
            int editData = JOptionPane.showConfirmDialog(null, komponenFormEdit, "Edit Data Transaksi", JOptionPane.OK_CANCEL_OPTION, 1);

            if (editData == 0) {
                if (kode1.getText().isEmpty() || namaCustomer1.getText().isEmpty() || tanggalTransaksi1.getDate() == null) {
                    JOptionPane.showMessageDialog(null, "mohon untuk isi semua form");
                } else {
                    try {
                        boolean status = false;
                        // cek apakah ada perbedaan di data kode sebelum dan sesudah
                        // jika ada perbedaan, maka masuk dikondisi ini
                        if (!valueInCell.equals(kode1.getText())) {
                            status = sqlTelur.checkKode(kode1.getText());
                        }

                        if (status) {
//                            System.out.println("Status " + status);
//                            System.out.println("Ada kode yg sama");
                            JOptionPane.showMessageDialog(null, "Ada Kode Yang Sama", "Alert !", JOptionPane.ERROR_MESSAGE);
                        } else {
//                            System.out.println("Status " + status);
//                            System.out.println("tidak ada kode yg sama");
                            try {
                                int rowUpdate = sqlTelur.UpdateTelur(getIdTelur, kode1.getText(), namaCustomer1.getText(), jenisTelur1.getSelectedItem().toString(), statusTelur1.getSelectedItem().toString(), jenisPembayaran1.getSelectedItem().toString(), sdf.format(tanggalTransaksi1.getDate()), hargaTelur1.getValue().hashCode(), jumlahTelur1.getValue().hashCode());
                                if (rowUpdate > 0) {
                                    JOptionPane.showMessageDialog(null, "Berhasil Diedit");
                                    // tampilkan data di JTable
                                    DisplayDataTables();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Something goes wrong", "Alert !", JOptionPane.ERROR_MESSAGE);
                                }
                            } catch (HeadlessException | ClassNotFoundException | SQLException e) {
                                System.out.println(e);
                            }
                        }
                    } catch (HeadlessException | ClassNotFoundException | SQLException e) {
                        System.err.println(e.getMessage());
                        JOptionPane.showMessageDialog(null, "Something goes wrong", "Alert !", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } else if (editData == 2) {
                System.out.println("Batal Edit");
            }
        } else if (hasilKonfirmasiEditHapus == 1) {
            // jika memilih menghapus data
            int hapusData = JOptionPane.showConfirmDialog(null, "Yakin Hapus Data Transaksi?", "Hapus Data Transaksi", JOptionPane.OK_CANCEL_OPTION, 1);
            if (hapusData == 0) {
                Point point = evt.getPoint();
                int row = tabelTelur.rowAtPoint(point);
                String valueInCell = (String) tabelTelur.getValueAt(row, 0);
                try {
                    int rowUpdate = sqlTelur.DeleteTelur(valueInCell);
                    if (rowUpdate > 0) {
                        JOptionPane.showMessageDialog(null, "Berhasil Dihapus");
                        // tampilkan data di JTable
                        DisplayDataTables();
                    } else {
                        JOptionPane.showMessageDialog(null, "Something goes wrong", "Alert !", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (HeadlessException | ClassNotFoundException | SQLException e) {
                    System.out.println(e);
                }

            } else if (hapusData == 2) {
                System.out.println("Batal Hapus");
            }
        }


    }//GEN-LAST:event_tabelTelurMousePressed

    private void eksporTabelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eksporTabelActionPerformed
        // TODO add your handling code here:
//        System.out.println("Export Table");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        DateTimeFormatter dtf_ttd = DateTimeFormatter.ofPattern("dd MM yyyy");
        LocalDateTime now = LocalDateTime.now();
        Document doc = new Document(PageSize.LEGAL.rotate());

        // Tentukan direktori awal (default directory)
        File defaultDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        // Buat instance JFileChooser dengan direktori awal
        JFileChooser chooser = new JFileChooser(defaultDirectory);
        // Hanya izinkan memilih file dengan ekstensi .pdf
        FileNameExtensionFilter filter = new FileNameExtensionFilter("PDF Files", "pdf");
        chooser.setFileFilter(filter);
        // Tentukan nilai awal untuk file name
        String initialFileName = "Export_" + PrintGenerateDate() + ".pdf";
        chooser.setSelectedFile(new File(initialFileName));

        PdfWriter writer;
        int returnValue = chooser.showSaveDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            // Pengguna telah memilih file/direktori
            File selectedFile = chooser.getSelectedFile();
            String selectedPath = selectedFile.getPath();

            // Validasi ekstensi file
            if (!selectedPath.toLowerCase().endsWith(".pdf")) {
//                System.out.println("File harus berformat PDF.");
                JOptionPane.showMessageDialog(null, "File harus berformat PDF.", "Alert !", JOptionPane.ERROR_MESSAGE);
            } else {
                // Proses ekspor file PDF
                try {
                    writer = PdfWriter.getInstance(doc, new FileOutputStream(selectedPath));
                    PdfHeader event = new PdfHeader();
                    writer.setPageEvent(event);
                    doc.open();
                    // "src\\assets\\logone.png"
                    Image img = Image.getInstance(MainFrame.class.getResource("/assets/logone.png"));
                    Paragraph teksTabel1 = new Paragraph("Export Tabel Transaksi");
                    teksTabel1.setAlignment(Element.ALIGN_CENTER);
                    img.setAbsolutePosition(-30f, 445f);

                    doc.add(img);
                    doc.add(new Paragraph("           Laporan Transaksi Telur CV. Nadira", FontFactory.getFont(FontFactory.HELVETICA_BOLD, 24)));
                    doc.add(new Paragraph(" "));
                    doc.add(new Paragraph(" "));
                    doc.add(new Paragraph(" "));
                    doc.add(new Paragraph("Tanggal Export: " + dtf.format(now)));
                    doc.add(teksTabel1);
                    doc.add(new Paragraph(" "));

                    PdfPTable table = new PdfPTable(9);
                    table.setWidthPercentage(100);

                    // mencetak kolom
                    String[] columnNames = {"Kode", "Nama", "Jenis Telur", "Status Telur", "Jenis Pembayaran", "Tanggal Transaksi", "Harga Telur", "Jumlah Telur", "Total Harga"};
                    for (String columnName : columnNames) {
                        table.addCell(createHeaderCell(columnName, 1, 1, Element.ALIGN_CENTER));
                    }

                    // Memindahkan isi JTable ke array 2 dimensi
                    int rowCount = tabelTelur.getRowCount();
                    int colCount = tabelTelur.getColumnCount();
                    String[][] dataArrayTelur = new String[rowCount][colCount];

                    for (int i = 0; i < rowCount; i++) {
                        for (int j = 0; j < colCount; j++) {
                            dataArrayTelur[i][j] = tabelTelur.getValueAt(i, j).toString();
                        }
                    }
                    // mencetak isi table
                    for (String[] row : dataArrayTelur) {
                        for (int i = 0; i < columnNames.length; i++) {
                            table.addCell(createCell(row[i], 1, 1, Element.ALIGN_LEFT));
                        }
                    }
                    doc.add(table);
                    doc.add(new Paragraph(" "));
                    doc.add(new Paragraph(" "));
                    doc.add(new Paragraph("Klaten, " + dtf_ttd.format(now), FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
                    doc.add(new Paragraph("Direktur CV. Nadira", FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
                    doc.add(new Paragraph(" "));
                    doc.add(new Paragraph(" "));
                    doc.add(new Paragraph(" "));
                    doc.add(new Paragraph("Puput Andika Prasasti", FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
                    // close the PDF file  
                    doc.close();
                    // closes the writer  
                    writer.close();
//                    System.out.println("PDF created.");
                    JOptionPane.showMessageDialog(null, "Tabel Transaksi Berhasil di-eksport");
//                    System.out.println("File berhasil diekspor.");
                } catch (DocumentException | HeadlessException | IOException ex) {
                    JOptionPane.showMessageDialog(null, ex, "Alert !", JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
//            System.out.println("Pengguna membatalkan pemilihan.");
            JOptionPane.showMessageDialog(null, "Tabel Transaksi Batal di-eksport");
        }

    }//GEN-LAST:event_eksporTabelActionPerformed

    private void tombolFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolFilterActionPerformed
        // TODO add your handling code here:
        String filter1 = null;
        if (filterTanggalTransaksi.getDate() != null) {
            filter1 = sdf.format(filterTanggalTransaksi.getDate());
        }
//        System.out.println("Filter tanggal = " + filter1);
        String filter2 = null;
        if (!filterJenisTelur.getSelectedItem().toString().equalsIgnoreCase("(Pilih Jenis Telur)")) {
            filter2 = filterJenisTelur.getSelectedItem().toString();
        }
//        System.out.println("Filter Jenis Telur = " + filter2);
        String filter3 = null;
        if (!filterStatusTelur.getSelectedItem().toString().equalsIgnoreCase("(Pilih Status Telur)")) {
            filter3 = filterStatusTelur.getSelectedItem().toString();
        }
//        System.out.println("Filter Status Telur = " + filter3);
        String filter4 = null;
        if (!filterJenisPembayaran.getSelectedItem().toString().equalsIgnoreCase("(Pilih Jenis Pembayaran)")) {
            filter4 = filterJenisPembayaran.getSelectedItem().toString();
        }
//        System.out.println("Filter Jenis Pembayaran = " + filter4);

        try {
            DefaultTableModel tableModel = new DefaultTableModel();
            String[] columnNames = {"Kode", "Nama Customer", "Jenis Telur", "Status Telur", "Jenis Pembayaran", "Tanggal Transaksi", "Harga Telur", "Jumlah Telur", "Total Harga Telur"};
            for (String columnName : columnNames) {
                tableModel.addColumn(columnName);
            }

            // proses filtering sesuai inputan yang diinginkan
            ArrayList<Telur> lists = sqlTelur.getDataByFilter(filter1, filter2, filter3, filter4);
            if (lists != null) {
                for (Telur telur : lists) {
                    Object[] row = {telur.getKode(),telur.getNamaCustomer(),telur.getJenisTelur(),telur.getStatusTelur(),telur.getJenisPembayaran(),telur.getTanggalTransaksi(),telur.getHargaTelur(),telur.getJumlahTelur(),telur.getTotalHarga()};
                    tableModel.addRow(row);
                }
                tabelTelur.setModel(tableModel);
            } else {
                // jika inputan null
                DisplayDataTables();
            }

        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }//GEN-LAST:event_tombolFilterActionPerformed

    private void buatLaporanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buatLaporanActionPerformed
        Document doc = new Document(PageSize.LEGAL.rotate());
        String listJenisTelur1[] = {"Coklat", "Putih"};
        JComboBox jenisTelur2 = new JComboBox(listJenisTelur1);
        JDateChooser tanggalMulaiTransaksi = new JDateChooser();
        JDateChooser tanggalSelesaiTransaksi = new JDateChooser();

        Object[] komponenFormLaporan = {
            "Jenis Telur:", jenisTelur2,
            "Tanggal Mulai:", tanggalMulaiTransaksi,
            "Tanggal Selesai:", tanggalSelesaiTransaksi
        };

        // menampilkan JOptionPane Dialog
        int konfirmasiLaporan = JOptionPane.showConfirmDialog(null, komponenFormLaporan, "Buat Laporan", JOptionPane.OK_CANCEL_OPTION, 1);
        if (konfirmasiLaporan == 0) {
            // jika ingin membuat laporan
            String tglMulai = sdf.format(tanggalMulaiTransaksi.getDate());
            String tglSelesai = sdf.format(tanggalSelesaiTransaksi.getDate());

            LocalDate localStartDate = tanggalMulaiTransaksi.getDate().toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();
            LocalDate localEndDate = tanggalSelesaiTransaksi.getDate().toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();

            // Hitung selisih hari antara kedua tanggal
            long selisihHari = ChronoUnit.DAYS.between(localStartDate, localEndDate);
//            System.out.println("selisih hari = " + selisihHari);
            if (selisihHari >= 0) {
                // Tentukan direktori awal (default directory)
                File defaultDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
                // Buat instance JFileChooser dengan direktori awal
                JFileChooser chooser = new JFileChooser(defaultDirectory);
                // Hanya izinkan memilih file dengan ekstensi .pdf
                FileNameExtensionFilter filter = new FileNameExtensionFilter("PDF Files", "pdf");
                chooser.setFileFilter(filter);
                // Tentukan nilai awal untuk file name
                String initialFileName = "Laporan_" + PrintGenerateDate() + ".pdf";
                chooser.setSelectedFile(new File(initialFileName));

                PdfWriter writer;
                int returnValue = chooser.showSaveDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    // Pengguna telah memilih file/direktori
                    File selectedFile = chooser.getSelectedFile();
                    String selectedPath = selectedFile.getPath();

                    // Validasi ekstensi file
                    if (!selectedPath.toLowerCase().endsWith(".pdf")) {
//                        System.out.println("File harus berformat PDF.");
                        JOptionPane.showMessageDialog(null, "File harus berformat PDF.", "Alert !", JOptionPane.ERROR_MESSAGE);
                    } else {
                        // Proses ekspor file PDF
                        try {
                            writer = PdfWriter.getInstance(doc, new FileOutputStream(selectedPath));
                            PdfHeader event = new PdfHeader();
                            writer.setPageEvent(event);
                            // opens the PDF  
                            doc.open();

                            // Header Laporan
                            // src\\assets\\logone.png
                            Image img = Image.getInstance(MainFrame.class.getResource("/assets/logone.png"));
                            Paragraph teksTabel1 = new Paragraph("Tabel Transaksi Masuk");
                            teksTabel1.setAlignment(Element.ALIGN_CENTER);
                            Paragraph teksTabel2 = new Paragraph("Tabel Transaksi Keluar");
                            teksTabel2.setAlignment(Element.ALIGN_CENTER);
                            Paragraph teksTabel3 = new Paragraph("Tabel Transaksi Reject");
                            teksTabel3.setAlignment(Element.ALIGN_CENTER);
                            img.setAbsolutePosition(-30f, 445f);
                            doc.add(img);
                            doc.add(new Paragraph("           Laporan Transaksi Telur CV. Nadira", FontFactory.getFont(FontFactory.HELVETICA_BOLD, 24)));
                            doc.add(new Paragraph(" "));
                            doc.add(new Paragraph(" "));
                            doc.add(new Paragraph(" "));
                            SimpleDateFormat sdf_laporan = new SimpleDateFormat("dd MM yyyy");
                            doc.add(new Paragraph("Tanggal Mulai: " + sdf_laporan.format(tanggalMulaiTransaksi.getDate())));
                            doc.add(new Paragraph("Tanggal Akhir: " + sdf_laporan.format(tanggalSelesaiTransaksi.getDate())));
                            doc.add(teksTabel1);
                            doc.add(new Paragraph(" "));
                            // adds paragraph to the PDF file
                            PdfPTable tableMasuk = new PdfPTable(9);
                            PdfPTable tableKeluar = new PdfPTable(9);
                            PdfPTable tableReject = new PdfPTable(9);
                            tableMasuk.setWidthPercentage(100);
                            tableKeluar.setWidthPercentage(100);
                            tableReject.setWidthPercentage(100);
                            int totalHargaMasuk = 0;
                            int totalHargaKeluar = 0;
                            int totalHargaReject = 0;

                            String[] columnNames = {"Kode", "Nama", "Jenis Telur", "Status Telur", "Jenis Pembayaran", "Tanggal Transaksi", "Harga Telur", "Jumlah Telur", "Total Harga"};
                            for (String columnName : columnNames) {
                                tableMasuk.addCell(createHeaderCell(columnName, 1, 1, Element.ALIGN_CENTER));
                            }
                            for (String columnName : columnNames) {
                                tableKeluar.addCell(createHeaderCell(columnName, 1, 1, Element.ALIGN_CENTER));
                            }
                            for (String columnName : columnNames) {
                                tableReject.addCell(createHeaderCell(columnName, 1, 1, Element.ALIGN_CENTER));
                            }

                            // Query Laporan Transaksi Masuk
                            ArrayList<Telur> listsMasuk = sqlTelur.getDataLaporan("Masuk", tglMulai, tglSelesai, jenisTelur2.getSelectedItem().toString());
                            if (listsMasuk != null) {
                                listsMasuk.stream().map((telur) -> {
                                    tableMasuk.addCell(createCell(telur.getKode(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableMasuk.addCell(createCell(telur.getNamaCustomer(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableMasuk.addCell(createCell(telur.getJenisTelur(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableMasuk.addCell(createCell(telur.getStatusTelur(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableMasuk.addCell(createCell(telur.getJenisPembayaran(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableMasuk.addCell(createCell(telur.getTanggalTransaksi(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableMasuk.addCell(createCell(Integer.toString(telur.getHargaTelur()), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableMasuk.addCell(createCell(Integer.toString(telur.getJumlahTelur()), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).forEachOrdered((telur) -> {
                                    tableMasuk.addCell(createCell(Integer.toString(telur.getTotalHarga()), 1, 1, Element.ALIGN_LEFT));
                                });
                            } else {
                                for (String columnName : columnNames) {
                                    tableMasuk.addCell(createCell("0", 1, 1, Element.ALIGN_LEFT));
                                }
                            }
                            doc.add(tableMasuk);
                            // Query hasil summarize Laporan Masuk
//                            Map<String, Integer> resultTelurMasuk = sqlTelur.getDataLaporanTotal("Masuk", tglMulai, tglSelesai, jenisTelur2.getSelectedItem().toString());
//                            doc.add(new Paragraph("Total Telur Masuk: " + resultTelurMasuk.get("sumJumlahTelur")));
//                            totalHargaMasuk = resultTelurMasuk.get("sumTotalHarga");
//                            doc.add(new Paragraph("Total Harga Pembelian: " + totalHargaMasuk));
//                            doc.add(new Paragraph(" "));
                            doc.add(new Paragraph("Total Telur Masuk: " + sqlTelur.getDataLaporanTotalJumlahTelur("Masuk", tglMulai, tglSelesai, jenisTelur2.getSelectedItem().toString())));
                            totalHargaMasuk = sqlTelur.getDataLaporanTotalTotalHarga("Masuk", tglMulai, tglSelesai, jenisTelur2.getSelectedItem().toString());
                            doc.add(new Paragraph("Total Harga Pembelian: " + totalHargaMasuk));
                            doc.add(new Paragraph(" "));

                            // Query Laporan Transaksi Keluar
                            doc.add(teksTabel2);
                            doc.add(new Paragraph(" "));
                            ArrayList<Telur> listsKeluar = sqlTelur.getDataLaporan("Keluar", tglMulai, tglSelesai, jenisTelur2.getSelectedItem().toString());
                            if (listsKeluar != null) {
                                listsKeluar.stream().map((telur) -> {
                                    tableKeluar.addCell(createCell(telur.getKode(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableKeluar.addCell(createCell(telur.getNamaCustomer(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableKeluar.addCell(createCell(telur.getJenisTelur(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableKeluar.addCell(createCell(telur.getStatusTelur(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableKeluar.addCell(createCell(telur.getJenisPembayaran(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableKeluar.addCell(createCell(telur.getTanggalTransaksi(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableKeluar.addCell(createCell(Integer.toString(telur.getHargaTelur()), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableKeluar.addCell(createCell(Integer.toString(telur.getJumlahTelur()), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).forEachOrdered((telur) -> {
                                    tableKeluar.addCell(createCell(Integer.toString(telur.getTotalHarga()), 1, 1, Element.ALIGN_LEFT));
                                });
                            } else {
                                for (String columnName : columnNames) {
                                    tableKeluar.addCell(createCell("0", 1, 1, Element.ALIGN_LEFT));
                                }
                            }
                            doc.add(tableKeluar);
                            // Query hasil summarize Laporan Keluar
//                            Map<String, Integer> resultTelurKeluar = sqlTelur.getDataLaporanTotal("Keluar", tglMulai, tglSelesai, jenisTelur2.getSelectedItem().toString());
//                            doc.add(new Paragraph("Total Telur Keluar: " + resultTelurKeluar.get("sumJumlahTelur")));
//                            totalHargaKeluar = resultTelurKeluar.get("sumTotalHarga");
//                            doc.add(new Paragraph("Total Harga Penjualan: " + totalHargaKeluar));
//                            doc.add(new Paragraph(" "));
                            doc.add(new Paragraph("Total Telur Keluar: " + sqlTelur.getDataLaporanTotalJumlahTelur("Keluar", tglMulai, tglSelesai, jenisTelur2.getSelectedItem().toString())));
                            totalHargaKeluar = sqlTelur.getDataLaporanTotalTotalHarga("Keluar", tglMulai, tglSelesai, jenisTelur2.getSelectedItem().toString());
                            doc.add(new Paragraph("Total Harga Penjualan: " + totalHargaKeluar));
                            doc.add(new Paragraph(" "));

                            // Query Laporan Transaksi Reject
                            doc.add(teksTabel3);
                            doc.add(new Paragraph(" "));
                            ArrayList<Telur> listsReject = sqlTelur.getDataLaporan("Reject", tglMulai, tglSelesai, jenisTelur2.getSelectedItem().toString());
                            if (listsReject != null) {
                                listsReject.stream().map((telur) -> {
                                    tableReject.addCell(createCell(telur.getKode(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableReject.addCell(createCell(telur.getNamaCustomer(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableReject.addCell(createCell(telur.getJenisTelur(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableReject.addCell(createCell(telur.getStatusTelur(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableReject.addCell(createCell(telur.getJenisPembayaran(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableReject.addCell(createCell(telur.getTanggalTransaksi(), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableReject.addCell(createCell(Integer.toString(telur.getHargaTelur()), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).map((telur) -> {
                                    tableReject.addCell(createCell(Integer.toString(telur.getJumlahTelur()), 1, 1, Element.ALIGN_LEFT));
                                    return telur;
                                }).forEachOrdered((telur) -> {
                                    tableReject.addCell(createCell(Integer.toString(telur.getTotalHarga()), 1, 1, Element.ALIGN_LEFT));
                                });
                            } else {
                                for (String columnName : columnNames) {
                                    tableReject.addCell(createCell("0", 1, 1, Element.ALIGN_LEFT));
                                }
                            }
                            doc.add(tableReject);
                            // Query hasil summarize Laporan Reject
//                            Map<String, Integer> resultTelurReject = sqlTelur.getDataLaporanTotal("Reject", tglMulai, tglSelesai, jenisTelur2.getSelectedItem().toString());
//                            doc.add(new Paragraph("Total Telur Reject: " + resultTelurReject.get("sumJumlahTelur")));
//                            totalHargaReject = resultTelurReject.get("sumTotalHarga");
//                            doc.add(new Paragraph("Total Harga Reject: " + totalHargaReject));
//                            doc.add(new Paragraph(" "));
                            doc.add(new Paragraph("Total Telur Reject: " + sqlTelur.getDataLaporanTotalJumlahTelur("Reject", tglMulai, tglSelesai, jenisTelur2.getSelectedItem().toString())));
                            totalHargaReject = sqlTelur.getDataLaporanTotalTotalHarga("Reject", tglMulai, tglSelesai, jenisTelur2.getSelectedItem().toString());
                            doc.add(new Paragraph("Total Harga Reject: " + totalHargaReject));
                            doc.add(new Paragraph(" "));

                            doc.add(new Paragraph("Total Laba (Keluar - Masuk - Reject): " + totalHargaKeluar + " - " + totalHargaMasuk + " - " + totalHargaReject + " = " + (totalHargaKeluar - totalHargaMasuk - totalHargaReject)));
                            doc.add(new Paragraph(" "));
                            doc.add(new Paragraph(" "));
                            DateTimeFormatter dtf_ttd = DateTimeFormatter.ofPattern("dd MM yyyy");
                            LocalDateTime now = LocalDateTime.now();
                            doc.add(new Paragraph("Klaten, " + dtf_ttd.format(now), FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
                            doc.add(new Paragraph("Direktur CV. Nadira", FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
                            doc.add(new Paragraph(" "));
                            doc.add(new Paragraph(" "));
                            doc.add(new Paragraph(" "));
                            doc.add(new Paragraph("Puput Andika Prasasti", FontFactory.getFont(FontFactory.HELVETICA_BOLD)));

                            // close the PDF file  
                            doc.close();
                            // closes the writer  
                            writer.close();
//                            System.out.println("PDF created.");
                            JOptionPane.showMessageDialog(null, "Laporan Transaksi berhasil di Generate!");
                        } catch (DocumentException | HeadlessException | IOException | ClassNotFoundException | SQLException ex) {
                            System.out.println(ex);
                        }
                    }
                } else {
//                    System.out.println("Pengguna membatalkan pemilihan.");
                    JOptionPane.showMessageDialog(null, "Tabel Transaksi Batal di-eksport");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Tanggal Selesai Wajib Setelah Tanggal Mulai", "Alert !", JOptionPane.ERROR_MESSAGE);
            }

        } else if (konfirmasiLaporan == 2) {
            System.out.println("Batal Buat Laporan");
        }
    }//GEN-LAST:event_buatLaporanActionPerformed

    private void PanduanPenggunaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PanduanPenggunaActionPerformed
        // TODO add your handling code here:
//        BantuanFrame banF = new BantuanFrame();
        BantuanUserManualForm banF = new BantuanUserManualForm();
        banF.setVisible(true);
    }//GEN-LAST:event_PanduanPenggunaActionPerformed
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new MainFrame().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem PanduanPengguna;
    private javax.swing.JMenuItem buatLaporan;
    private javax.swing.JMenuItem eksporTabel;
    private javax.swing.JComboBox<String> filterJenisPembayaran;
    private javax.swing.JComboBox<String> filterJenisTelur;
    private javax.swing.JComboBox<String> filterStatusTelur;
    private com.toedter.calendar.JDateChooser filterTanggalTransaksi;
    private javax.swing.JSpinner hargaTelur;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JComboBox<String> jenisPembayaran;
    private javax.swing.JComboBox<String> jenisTelur;
    private javax.swing.JSpinner jumlahTelur;
    private javax.swing.JTextField kode;
    private java.awt.Label label1;
    private javax.swing.JTextField namaCustomer;
    private javax.swing.JComboBox<String> statusTelur;
    private javax.swing.JTable tabelTelur;
    private com.toedter.calendar.JDateChooser tanggalTransaksi;
    private javax.swing.JButton tombolFilter;
    private javax.swing.JButton tombolTambah;
    // End of variables declaration//GEN-END:variables
}
